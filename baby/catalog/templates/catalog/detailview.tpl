{% include 'base.tpl' %}
{% load static %}
{% block head %}<style>@font-face { font-family: ALSRubl;  src: url(/static/rouble.ttf);}</style>{% endblock %}
{% block pagecontent %}
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img class="img-thumbnail" src="{% for i in object.Images.all %}{{ i.name.url }}{% endfor %}">
            </div>
            <div class="col-md-7">
                <h3>{{ object.name }}</h3>
                <div>{{ object.description | linebreaks}}</div>
                <p>Страна производитель: {{ object.madein }}</p>
                <p>Выберите размер:
                    <select id="sizes">
                        <option value="" selected>---</option>
                        {% for s in object.Size.all %}
                            <option value="{{ s.size.ident }}">{{ s.size.name }}</option>
                        {% endfor %}
                    </select>
                </p>
                <p>Количество: <input id='qty' type="number" min="1" max="" value="1"> <span class="badge" id="sklad"></span></p>
                <button class="btn btn-warning" onclick="add_to_cart()"><span class="glyphicon glyphicon-shopping-cart"></span> Добавить в корзину</button>
            </div>
        </div>
    </div>






    <script>
        $('#sizes').change(function(){
            $.ajax({
                type: "POST",
                url: "{% url 'post' %}",
                data: { 'size': $("#sizes option:selected").text(),
                        'csrfmiddlewaretoken': "{{ csrf_token }}",
                        'prodid': {{ object.id }}
                },
                success: function(data) {
                    $('#qty').attr('max', data);
                    $('#sklad').html('Всего на складе: ' + data + ' шт.');
                }
            })
        });
        function add_to_cart() {
            $.ajax({
              type: "POST",
              url: "{% url 'add_to_cart' %}",
              data: {'price': {{ object.price }},
                     'csrfmiddlewaretoken': "{{ csrf_token }}",
                     'prodid': {{ object.id }},
                     'size': $("#sizes option:selected").text(),
                     'qty': $('#qty').val()
              }
            });
        }
    </script>
{% endblock %}