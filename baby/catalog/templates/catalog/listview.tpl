{% include 'base.tpl' %}
{% load static %}
{% block head %}<style>@font-face { font-family: ALSRubl;  src: url(/static/rouble.ttf);}</style>{% endblock %}
{% block pagecontent %}
    <div class="container" style="text-align: center;">
        {% for obj in object_list %}
            <div style="border: 1px solid #e5e5e5; margin: 0.75%; padding: 10px 30px 30px 30px; width: 23.5%; float: left; background-color: white">
                <a href="{% url 'product-detail' slug=obj.male.slug pk=obj.id type=obj.category.ident %}">
                    <img src="{%for a in obj.Images.all%} {{ a.name.url }} {% endfor %}" width="150">
                </a>
                <h4 style="border-top: 1px solid #e5e5e5; clear: both;font-size: 18px; font-weight: normal; padding: 20px 0 0 0; text-transform: none">
                    {{ obj.name }}
                </h4>
                <div>
                    {{ obj.price }} <span style="font-family: ALSRubl;">a</span>
                </div>
            </div>
        {% endfor %}
    </div>
{% endblock %}