from django.contrib import admin
from .models import *


class ProductImageInline(admin.TabularInline):
    model = ProductImages
    extra = 1

class ProductSizeQtyInline(admin.TabularInline):
    model = ProductSize
    extra = 1

class ProductAdmin(admin.ModelAdmin):
    inlines = [ProductSizeQtyInline, ProductImageInline]

# Register your models here.
admin.site.register(CategoryName)
admin.site.register(Brand)
admin.site.register(Size)
admin.site.register(Product, ProductAdmin)
admin.site.register(Male)