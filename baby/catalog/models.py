from django.db import models

class Male(models.Model):
    male = models.CharField('Пол', max_length=1)
    slug = models.CharField(max_length=10, null=True)

    def __str__(self):
        return self.male

class CategoryName(models.Model):
    ident = models.CharField('Идентификатор', max_length=50)
    name = models.CharField('Название категории', max_length=256)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name

class Brand(models.Model):
    ident = models.CharField('Идентификатор', max_length=50)
    name = models.CharField('Название фирмы', max_length=256)

    class Meta:
        verbose_name = 'Фирма'
        verbose_name_plural = 'Фирмы'

    def __str__(self):
        return self.name


class Size(models.Model):
    ident = models.CharField('Идентификатор', max_length=50)
    name = models.CharField('Название размера', max_length=256)

    class Meta:
        verbose_name = 'Размер'
        verbose_name_plural = 'Размеры'

    def __str__(self):
        return self.name


class Product(models.Model):
    ident = models.CharField('Идентификатор', max_length=50, blank=True)
    name = models.CharField('Название продукта', max_length=256)
    male = models.ForeignKey('Male', verbose_name='Пол', null=True)
    category = models.ForeignKey(CategoryName, verbose_name='Категория')
    brand = models.ForeignKey(Brand, verbose_name='Фирма')
    madein = models.CharField('Страна производитель', max_length=50, default='Россия')
    description = models.TextField('Описание', blank=True)

    price = models.IntegerField('Цена', blank=True, default=0)

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def __str__(self):
        return self.name

    def icon(self):
        return self.Images.get(first=True).name.url

class ProductImages(models.Model):
    product = models.ForeignKey(Product, verbose_name='Продукт', related_name='Images')
    name = models.ImageField('Название картинки', upload_to='images/products', max_length=256)
    first = models.BooleanField('Сделать иконкой', default=False)

    class Meta:
        verbose_name = 'Изображение товара'
        verbose_name_plural = 'Изображения товара'

    def __str__(self):
        return self.product.name


class ProductSize(models.Model):
    product = models.ForeignKey(Product, related_name='Size')
    size = models.ForeignKey(Size, verbose_name='Размер')
    qty = models.IntegerField('Количество')

    class Meta:
        verbose_name = 'Размер товара'
        verbose_name_plural = 'Размеры товара'

    def __str__(self):
        return self.product.name



