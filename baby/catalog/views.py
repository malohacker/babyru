from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView, DetailView

from catalog.models import Product


class ProductList(ListView):
    template_name = 'catalog/listview.tpl'

    def get_queryset(self):
        return Product.objects.filter(category__ident=self.kwargs['type'], male__slug=self.kwargs['slug'])


class ProductDetail(DetailView):
    template_name = 'catalog/detailview.tpl'
    model = Product


def post(request):
    product = Product.objects.get(id=request.POST.get('prodid'))
    return HttpResponse(product.Size.get(size__ident=request.POST.get('size')).qty)
