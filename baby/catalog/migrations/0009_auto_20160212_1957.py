# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-12 19:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0008_auto_20160211_1741'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productimages',
            name='name',
            field=models.ImageField(max_length=256, upload_to='images/products', verbose_name='Название картинки'),
        ),
    ]
