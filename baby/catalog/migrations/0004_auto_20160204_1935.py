# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-04 19:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0003_auto_20160131_2008'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productimages',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Images', to='catalog.Product', verbose_name='Продукт'),
        ),
    ]
