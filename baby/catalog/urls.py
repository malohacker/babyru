from django.conf.urls import url, include
from django.contrib import admin

from catalog.views import *
from home.views import *

urlpatterns = [
    url(r'^(?P<slug>[-\w]+)/(?P<type>[-\w]+)/$', ProductList.as_view(), name='product-list'),
    url(r'^(?P<slug>[-\w]+)/(?P<type>[-\w]+)/(?P<pk>[-\w]+)/$', ProductDetail.as_view(), name='product-detail'),
    url(r'^post/', post, name='post')
]