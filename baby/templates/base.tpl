<!DOCTYPE html>

{% load static %}

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Интернет-магазин</title>

        <link rel="stylesheet" href="{% static 'bootstrap/css/bootstrap.min.css' %}">

        <script src="{% static 'js/jquery.js' %}"></script>
        <script src="{% static 'bootstrap/js/bootstrap.min.js' %}"></script>
        {% block head %}{% endblock %}
        <style>@font-face { font-family: ALSRubl;  src: url(/static/rouble.ttf);}</style>
    </head>

    <body style="padding-top: 70px;">
        <!--Навигация-->

        <nav id="navbar-example" class="navbar navbar-default navbar-fixed-top" style="margin-bottom: 0">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-example-js-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{% url 'index' %}">Project Name</a>
                </div>
                <div class="collapse navbar-collapse bs-example-js-navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Мальчикам <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="drop1">
                                {% for category in categorys %}
                                    <li><a href="{% url 'product-list' slug='boys' type=category.ident %}">{{ category.name }}</a></li>
                                {% endfor %}
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a id="drop2" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Девочкам <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="drop2">
                                {% for category in categorys %}
                                    <li><a href="{% url 'product-list' slug='girls' type=category.ident %}">{{ category.name }}</a></li>
                                {% endfor %}
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a id="drop3" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="glyphicon glyphicon-shopping-cart"></span> Корзина: {{ request.session.cart.allprice }} <span style="font-family: ALSRubl;">a</span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="drop3">
                                {% for prod in request.session.cart.products %}
                                    <li><a><img height="40px" src="{{ prod.product.icon }}">{{ prod.product.name }} - {{ prod.product.price }}<span style="font-family: ALSRubl;">a</span></a></li>
                                {% endfor %}
                                <li role="separator" class="divider"></li>
                                <li><a href="{% url 'cart_detail' %}" class="btn btn-warning">Открыть корзину</a></li>
                            </ul>
                        </li>

                    </ul>

                </div>
            </div>
        </nav>
        <!--ТЕЛО-->
        {% block pagecontent %}{% endblock %}


    </body>
</html>