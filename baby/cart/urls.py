from django.conf.urls import url, include
from django.contrib import admin

from cart.views import *

urlpatterns = [
    url(r'^post/', add_to_cart, name='add_to_cart'),
    url(r'', CartView.as_view(), name='cart_detail')
]