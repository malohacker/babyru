{% extends 'base.tpl' %}

{% block pagecontent %}
    <div class="container">
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th>Размер</th>
                    <th>Количество</th>
                    <th>Цена</th>
                </tr>
            </thead>
            <tbody>
                {% for i in session.products %}
                    <tr>
                        <td><img height="150px" src="{{ i.product.icon }}"></td>
                        <td>{{ i.product.name }}</td>
                        <td>{{ i.product.description | linebreaks }}</td>
                        <td>{{ i.size }}</td>
                        <td>{{ i.qty }}</td>
                        <td>{{ i.product.price }}</td>
                    </tr>
                {% endfor %}
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <th>Итого: {{ session.allprice }} </th>
                    </tr>
            </tbody>
        </table>
    </div>
{% endblock %}