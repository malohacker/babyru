from django.http import HttpResponse
from django.views.generic import TemplateView

from catalog.models import Product

class CartView(TemplateView):
    template_name = 'cart/cart_detail.tpl'

    def get_context_data(self, **kwargs):
        context = super(CartView, self).get_context_data(**kwargs)
        context['session'] = self.request.session['cart']
        return context

def add_to_cart(request):
    request.session.modified = True
    pr = Product.objects.get(id=request.POST.get('prodid'))
    z = request.session['cart']
    z['products'].append({'product': pr, 'size': request.POST.get('size'), 'qty': request.POST.get('qty')})
    z['allprice'] += pr.price
    return HttpResponse('ок')
