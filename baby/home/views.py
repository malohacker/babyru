from django.http import HttpResponse
from django.views.generic import TemplateView

from catalog.models import Product, CategoryName


class MainView(TemplateView):
    template_name = 'home/homepage.tpl'

    @classmethod
    def last_fore(cls):
        z = []
        for cn in CategoryName.objects.all():
            n = Product.objects.filter(category__ident=cn.ident).order_by('-id').first()
            if n is not None:
                z.append(n)
        return z

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        context['last_five'] = self.last_fore()
        return context


def my_context_processor(request):
    if 'cart' not in request.session:
        request.session['cart'] = {'products': [], 'allprice': 0}
    vars = {
        'products': Product.objects.all(),
        'categorys': CategoryName.objects.all(),
    }
    return vars
