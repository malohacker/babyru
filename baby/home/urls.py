from django.conf.urls import url, include
from django.template.context_processors import request

from home.views import MainView

urlpatterns = [
    url(r'', MainView.as_view(), name='index'),
]