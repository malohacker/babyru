{% include 'base.tpl' %}
{% load static %}
{% block head %}<style>@font-face { font-family: ALSRubl;  src: url(/static/rouble.ttf);}</style>{% endblock %}
{% block pagecontent %}

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="margin-top: -17px">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div style="background: url({% static 'img/img.jpg' %}); height: 350px;"></div>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div style="background: url('/static/img/preview.jpg')">
        <div class="container" style="text-align: center;">
            <h3>НОВЫЕ ПОСТУПЛЕНИЯ</h3>
            {% for obj in last_five %}
                <div style="border: 1px solid #e5e5e5; margin: 0.75%; padding: 10px 30px 30px 30px; width: 23.5%; float: left; background-color: white">
                    <div style="height: 130px; display: inline-flex">
                    <a href="{% url 'product-detail' slug=obj.male.slug type=obj.category.ident pk=obj.id %}" style="display: flex; align-items: center">
                        <img style="max-height: 120px" src="{%for a in obj.Images.all%} {{ a.name.url }} {% endfor %}">
                    </a>
                    </div>
                    <h4 style="border-top: 1px solid #e5e5e5; clear: both;font-size: 18px; font-weight: normal; padding: 20px 0 0 0; text-transform: none">
                        {{ obj.name }}
                    </h4>
                    <div>
                        {{ obj.price }} <span style="font-family: ALSRubl;">a</span> {{ v }}
                    </div>
                </div>
            {% endfor %}
        </div>
{% endblock %}